#!/usr/bin/env bash

hostip=$(hostname -I)

raspip=$(nmap -sP ${hostip::-1}/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}')

ssh  pi@$raspip